# PPAI-DSI-3K3-GT-2018
> Repositorio del Proyecto general sobre Enterprise Architect
_____
### IMPORTANTE:
Antes que nada **RECUERDEN AVISAR EN EL GRUPO EL COMIENZO Y FIN DE EDICION**.

El mecanismo de Trabajo va a ser el siguiente:
A partir de esta actualizacion, todos deben hacer los **commits** sobre el *master*, es decir NO sobre los *branches* personales. **PERO**, deben avisar en el grupo de Telegram del Repositorio [REP] que tienen el proyecto bajo trabajo, para evitar que alguien mas se ponga a editar el trabajo y luego tengamos 2 puntos de partida diferente. Cualquier duda me consultan (Cyro).

En el caso de que hayan hecho una modificacion grande, y no estan seguros si esta bien, o quieren consultar algo, abran un *branch* que se llame **Consulta** o **Alternativo**, y ahi lo vemos entre todos. En caso de que este bien o que a la mayoria le parezca correcto lo agregamos al *master*.
_____
[Documento en Google Docs](https://goo.gl/xVbtLu)
_____
>>Bottacin - Casuscelli - Madrid - Loto - Rubiano - Soria
_____
`TelegramBot=Enabled`

-> GitLab Mudado